-- Created by Belal Khan (https://www.simplifiedcoding.net)

-- tables
-- Table assignments
CREATE TABLE assignments (
    id int  NOT NULL  AUTO_INCREMENT,
    name varchar(100)  NOT NULL,
    details varchar(1000)  NOT NULL,
    completed bool  NOT NULL  DEFAULT false,
    faculties_id int  NOT NULL,
    students_id int  NOT NULL,
    CONSTRAINT assignments_pk PRIMARY KEY (id)
);

-- Table faculties
CREATE TABLE faculties (
    id int  NOT NULL  AUTO_INCREMENT,
    name varchar(100)  NOT NULL,
    username varchar(100)  NOT NULL,
    password varchar(100)  NOT NULL,
    subject varchar(100)  NOT NULL,
    api_key varchar(100)  NOT NULL,
    CONSTRAINT faculties_pk PRIMARY KEY (id)
);

-- Table students
CREATE TABLE students (
    id int  NOT NULL  AUTO_INCREMENT,
    name varchar(100)  NOT NULL,
    username varchar(100)  NOT NULL,
    password varchar(100)  NOT NULL,
    api_key varchar(100)  NOT NULL,
    CONSTRAINT students_pk PRIMARY KEY (id)
);


-- foreign keys
-- Reference:  assignments_faculties (table: assignments)

ALTER TABLE assignments ADD CONSTRAINT assignments_faculties FOREIGN KEY assignments_faculties (faculties_id)
    REFERENCES faculties (id);
-- Reference:  assignments_students (table: assignments)

ALTER TABLE assignments ADD CONSTRAINT assignments_students FOREIGN KEY assignments_students (students_id)
    REFERENCES students (id);



-- End of file.